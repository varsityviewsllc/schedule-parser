'use strict';

process.argv[2] = 'production';
require('shelljs/global');

const
	fs = require('fs'),
	path = require('path'),

	config = require('../config/config.es6');

(() => {
	process.chdir(config.output);
	const gitRoot = exec('git rev-parse --show-toplevel', {silent: true}).output.trim();
	const curPath = pwd().trim();

	if (gitRoot == curPath) {
		exec('git add -A');
		exec('git commit -m "heroku update"');
		exec('git push --force --progress heroku master');
	} else console.log(`${config.output} must be a separate heroku repository!`);
})();