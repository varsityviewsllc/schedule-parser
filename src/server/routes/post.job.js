'use strict';

const
	config = require('../../config/config.es6'),
	db = require('../utils/redis.es6')('db'),
	store = require('../worker/store');

module.exports = {
	'/api/v1/job': function* () {
		let status = yield store.getStatus();
		
		if (status !== 'working') {
			let query = this.request.query;
			
			yield store.setStatus('queued');
			yield db.publish(`worker-${config.instanceUuid}`, JSON.stringify({
				task: 'job.start',
				inputList: Array.isArray(query.inputList) ? query.inputList : [query.inputList],
				isStrictMatch: query.isStrictMatch === 'true',
				databaseName: query.databaseName
			}));
			this.body = {status: 'OK'};
		} else {
			this.body = {status: 'ALREADY_WORKING'};
		}
	}
};
