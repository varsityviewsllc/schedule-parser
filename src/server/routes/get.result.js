'use strict';

const
	chan = require('chan'),
	json2csv = require('json2csv'),
	store = require('../worker/store');

module.exports = {
	'/api/v1/result': function* () {
		const fields = ['Date', 'Sport', 'Gender', 'Level', 'Home', 'Opponent', 'Time', 'Location', 'Comment', 'Summary', 'Description', 'Url'];

		let events = yield store.getEvents();

		let csvEventsData = events
			.map(event =>({
				Date: event.date,
				Sport: event.sport,
				Gender: event.gender,
				Level: event.level,
				Home: event.home,
				Opponent: event.opponent,
				Time: event.time,
				Location: event.location,
				Comments: event.comments,
				Summary: event.summary,
				Description: event.description,
				Url: event.url
			}));


		let csvChan = chan();
		json2csv({ data: csvEventsData, fields: fields }, csvChan);

		this.body = yield csvChan;
	}
};
