'use strict';

const
	path = require('path'),
	fs = require('fs');

module.exports = {
	'/api/v1/info': function* () {
		let appPackage = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../package.json')));
		this.body = {
			version: appPackage.version
		};
	}
};
