'use strict';

const
	config = require('../../config/config.es6'),
	db = require('../utils/redis.es6')('db');

module.exports = {
	'/api/v1/captcha': function* () {
		let code = this.request.query.code;

		yield db.publish(`worker-${config.instanceUuid}`, JSON.stringify({
			task: 'captcha.code',
			code
		}));
		this.body = {status: 'OK'};
	}
};
