'use strict';

const
	utils = require('../utils/utils');

const
	pluginNames = ['8to18.com', 'oneschoolstreet.com', 'roeing.com', 'rschooltoday.com', 'schedulestar.com'];

module.exports = {
	'/api/v1/plugin': function* () {
		this.body = {
			list: utils.toMap(pluginNames.map(name => require(`../plugins/${name}/index`)), e => e.name)
		};
	}
};
