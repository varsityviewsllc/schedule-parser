'use strict';

const
	config = require('../../config/config.es6'),
	subscriber = require('../utils/redis.es6')('subscriber'),
	store = require('../worker/store');

let captcha = '';

subscriber.on('message', function (channel, message) {
	message = JSON.parse(message);
	console.log('Received message: ', message);

	switch (message.task) {
		case 'captcha':
			captcha = message.captcha;

			break;
		default:
			console.error('web got undefined redis task', message.task);
			break;
	}
});
subscriber.subscribe(`web-${config.instanceUuid}`);

module.exports = {
	'/api/v1/job-status': function* () {
		let status = yield store.getStatus();
		let statusBoard = yield store.getStatusBoard();

		this.body = {
			status: status,
			statusBoard: statusBoard,
			captcha
		};
	}
};
