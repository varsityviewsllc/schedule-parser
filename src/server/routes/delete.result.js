'use strict';

const
	store = require('../worker/store');

module.exports = {
	'/api/v1/result': function* () {
		yield store.clean();
		this.body = {status: 'OK'};
	}
};
