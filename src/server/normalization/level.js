'use strict';

const
	utils = require('../utils/utils');

let levelRegexes = (() => {
	const levelNames = [
		'VARSITY', 'FRESHMAN', 'SOPHOMORE', 'JUNIOR HIGH', 'JUNIOR', 'SENIOR',
		'VAR', 'V', 'JV2', 'JV', 'JH', 'SO', 'HS', 'MS', 'F-S', 'FR', 'FA', 'FB', 'F',
		'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
		'1ST', '2ND', '3RD', '4TH', '5TH', '6TH', '7TH', '8TH', '9TH', '10TH', '11TH'
	];

	let levelSeparator = '[\\s\\(\\)\\/\\\\|-]+';

	return levelNames.map(levelName => ({
		name: levelName,
		regex: new RegExp(levelSeparator + levelName + levelSeparator, 'ig')
	}));
})();

const sameLevelNames = {
	'Frosh': ['F-S'],
	'Fresh(FA)': ['FA'],
	'Fresh(FB)': ['FB'],
	'Varsity': ['V', 'VAR'],
	'Sophomore': ['SO'],
	'Junior Varsity': ['JV'],
	'Junior High': ['JH'],
	'Freshman': ['FR', 'F', 'FA'],
	'1st': ['1'],
	'2nd': ['2'],
	'3rd': ['3'],
	'4th': ['4'],
	'5th': ['5'],
	'6th': ['6'],
	'7th': ['7'],
	'8th': ['8'],
	'9th': ['9'],
	'10th': ['10'],
	'11th': ['11']
};

module.exports = (level) => {
	level = level ? level.trim() : '';
	let levels = [];

	{
		let levelTransformed = ' ' + level + ' ';
		for (let r of levelRegexes) {
			let newLevel = levelTransformed.replace(r.regex, ' ');
			if (newLevel !== levelTransformed) {
				let levelName = utils.normalizeCase(r.name);
				for(let realLevelName of Object.keys(sameLevelNames)) {
					if (sameLevelNames[realLevelName].indexOf(r.name) > -1) {
						levelName = realLevelName;
						break;
					}
				}

				levels.push(levelName);
				levelTransformed = newLevel;
			}
		}
	}

	return levels.length > 0 ? levels.join(', ') : level;
};
