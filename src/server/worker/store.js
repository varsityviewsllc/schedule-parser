'use strict';

const
	co = require('co'),
	chan = require('chan'),
	utils = require('../utils/utils'),
	mongo = require('../utils/mongo.es6'),
	db = require('../utils/redis.es6')('db');

let results = chan();

module.exports = {
	setStatus: (status) => function* (){
		yield db.set('status', status);
	},

	getStatus: () => function* () {
		return yield db.get('status');
	},

	storeSchoolEvents: (entry) => results(entry),

	clean: () => function* (){
		yield db.flushdb();

		let ch = chan();
		let eventsCollection = mongo.collection('events');
		eventsCollection.remove({}, ch);
		yield ch;
	},

	getStatusBoard: () => function* (){
		let processedSchools = yield db.zrange('job-schools', 0, -1);

		let statusBoard = yield processedSchools.map(school => ({
			school: school,
			status: db.hmget(`job-status-${school}`, `dataFormat`, `name`, `status`, `events`, `duplicates`)
		}));

		return statusBoard.map(e => ({
			dataFormat: e.status[0],
			name: e.status[1],
			status: e.status[2],
			events: e.status[3],
			duplicates: e.status[4]
		}));
	},

	getEvents: () => function* (){
		let eventsCollection = mongo.collection('events');

		let eventsCh = chan();
		eventsCollection.find({}, eventsCh);
		return yield eventsCh;
	}
};

co(function* (){
	const sportsBlackList = ['administration'];
	const cancelledLabelsList = ['canceled', 'cancelled'];

	let eventsCollection = mongo.collection('events');
	eventsCollection.createIndex({date: 1, timeKey: 1, sport: 1, home: 1, opponent: 1}, {unique: true});

	while (true) {
		try {
			let r = yield results;
			let dataFormat = r.dataFormat, name = r.name, status = r.status, events = r.events;

			let isNew = yield db.hset(`job-status-${name}`, `name`, name);
			let schoolCounter = 1;
			if (isNew) {
				schoolCounter = yield db.incr(`school-counter`);
			}


			db.multi();
			db.hlen(`events`);

			let bulks = [];
			let counter = 0;
			let total = 0;

			if (isNew) {
				db.hset(`job-status-${name}`, `events`, 0);
				db.hset(`job-status-${name}`, `duplicates`, 0);
				db.zadd('job-schools', schoolCounter, name);
			}
			db.hset(`job-status-${name}`, `dataFormat`, dataFormat);
			db.hset(`job-status-${name}`, `status`, status);

			if (status === 'OK' && events && events.length > 0) {
				for (let event of events) {
					if (!event)
						continue;

					event.name = event.name ? event.name.trim() : '';
					event.sport = event.sport ? utils.normalizeCase(event.sport.trim()) : '';
					event.level = event.level ? utils.normalizeCase(event.level.trim()) : '';
					event.gender = event.gender ? utils.normalizeCase(event.gender.trim()) : '';
					event.location = event.location ? event.location.trim() : '';

					event.comments = event.comments ? event.comments.trim() : '';
					event.summary = event.summary ? event.summary.trim() : '';
					event.description = event.description ? event.description.trim() : '';
					event.url = event.url ? event.url.trim() : '';

					let sportLowerCase = event.sport.toLowerCase();
					let nameLowerCase = event.name.toLowerCase();
					if (sportsBlackList.indexOf(sportLowerCase) > -1 || cancelledLabelsList.some(label => nameLowerCase.includes(label)))
						continue;

					let start = new Date(Date.parse(event.startDateTime));

					let eventData = {
						date: utils.formatDate(start),
						sport: event.sport,
						gender: event.gender,
						level: event.level,
						time: event.isTimeDefined ? utils.formatTime(start) : '',
						timeKey: event.isTimeDefined ? utils.formatTime(start) : utils.getRandomString(16),
						location: event.location,
						comments: event.comments,
						summary: event.summary,
						description: event.description,
						url: event.url
					};

					if (event.home && event.opponent) {
						eventData.home = event.home;
						eventData.opponent = event.opponent;
					} else {
						eventData.home = event.isHome ? event.name : name;
						eventData.opponent = event.isHome ? name : event.name;
					}

					if (counter < 1)
						bulks.push(eventsCollection.initializeUnorderedBulkOp());

					total++;
					counter++;
					if (counter >= 999)
						counter = 0;

					bulks[bulks.length - 1].insert(eventData);
				}
			}

			yield db.exec();

			if (bulks.length > 0) {
				let inserted = 0;

				let ch = chan();
				for (let bulk of bulks) {
					bulk.execute(ch);

					let res = yield ch;
					inserted += res.nInserted;
				}

				yield db.hincrby(`job-status-${name}`, `events`, inserted);
				yield db.hincrby(`job-status-${name}`, `duplicates`, total - inserted);
			}
		} catch (err) {
			console.error('worker store error: ', err.stack);
		}
	}
});
