'use strict';

module.exports = (list, allowedList, isStrictMatch) => {
	let allowedListLowerCase = allowedList.map(e => e.toLowerCase());
	let allowedListWordsLowerCase = allowedListLowerCase.map(e => {
		console.log(e, e.match(/\S+/g));
		return e.match(/\S+/g).map(ee => ee.trim());
	});

	let filter = isStrictMatch
		? (schoolName) => allowedListLowerCase.some(e => schoolName === e)
		: (schoolName) => {
			return allowedListWordsLowerCase.some(e => e.every(ee => schoolName.includes(ee)));
		};

	return list.filter(school => {
		return school.id && school.name && filter(school.name.toLowerCase());
	});
};
