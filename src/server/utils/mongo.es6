'use strict';

const
	url = require('url'),
	mongo = require('mongojs');

if (!process.env.MONGO_URL) {
	console.error('MONGO_URL environment variable required!');
	process.exit(2);
}

let db = mongo(process.env.MONGO_URL, ['test'], {authMechanism: 'ScramSHA1'});

module.exports = db;
