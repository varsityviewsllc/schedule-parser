'use strict';

const
	co = require('co'),
	request = require('request-promise');

module.exports = (url, timeout) => {
    const userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36';

    const headers = {
        'User-Agent': userAgent
    };

    return request.get({
        uri: url,
        timeout: timeout,
        resolveWithFullResponse: true,
        headers
    });
};