'use strict';

const
	sleep = require('co-sleep'),
	co = require('co');

Date.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
    return Date.isLeapYear(this.getFullYear());
};

Date.prototype.getDaysInMonth = function () {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    let n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};

function Utils() {
	const self = this;

	this.normalizeCase = (name) => name.substr(0, 1).toUpperCase() + name.substr(1).toLowerCase();

	this.def = (call, def) => {
		try {
			return call();
		} catch(err) {
			return def;
		}
	};

	this.formatDate = (date) => {
		let dd = date.getDate();
		let mm = date.getMonth() + 1;

		let yyyy = date.getFullYear();
		if (dd < 10) {
			dd = '0' + dd;
		}

		if (mm < 10) {
			mm = '0' + mm;
		}

		return `${mm}.${dd}.${yyyy}`;
	};

	this.getRandomString = (len) => {
		let text = '';
		let possible = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`;

		for( let i = 0; i < len; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	};

	this.formatTime = (date) => {
		let r = date.toUTCString().match(/(\d+):(\d+):(\d+)/).slice(1);
		let hours = r[0], minutes = r[1];
		hours = parseInt(hours);
		minutes = parseInt(minutes);

		let ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12;
		minutes = minutes < 10 ? '0' + minutes : minutes;

		return hours + ':' + minutes + ' ' + ampm;
	};

	this.uniq = (array, key) => {
		if (!key)
			key = c => c;

		return [...array.reduce((map, c) => {
			map.set(key(c), c);
			return map;
		}, new Map()).values()];
	};

	this.uniqMap = (array, map) => self.uniq(array.map(map));

	this.toArray = (obj) => {
		return Object.keys(obj).reduce((a, k) => {
			a.push(obj[k]);
			return a;
		}, []);
	};

	this.toMap = (array, key, map) => {
		if (!key)
			key = (e) => e.id;
		if (!map)
			map = (e) => e;

		return array.reduce((a, t) => {
			a[key(t)] = map(t);
			return a;
		}, {});
	};

	this.sleep = (time) => co(function* (){
		yield sleep(time);
	});

    this.getDateRange = () => {
        let begin = new Date();
        begin.addMonths(-1);
        let end = new Date();
        end.addMonths(4);

        return [begin, end];
    };

	this.getCurrentSchoolYear = () => {
	    let d = new Date();
	    let m = d.getMonth();
	    let y = d.getFullYear();

	    if (m <= 6) {
	        return y - 1;
	    }
	    return y;
	};
}

module.exports = new Utils();
