'use strict';

const
	spawn = require('child_process').spawn,
	co = require('co'),
	chan = require('chan');

module.exports = (url) => {
	let casper = spawn('casperjs', ['tc.js', url]);

	let str = '';
	let state = 'image';

	let comm = {
		captchaCh: chan(),
		codeCh: chan(),
		resultCh: chan()
	};

	casper.stdout.on('data', (data) => {
		data = data.toString('utf8').trim();
		str += data;
		let match = str.match(/!(.*)!/);

		if (!match)
			return;
		match = match[1];

		switch (state) {
			case 'image':
			{
				co(function* () {
					yield comm.captchaCh(match);
					let code = yield comm.codeCh;
					casper.stdin.write(code + '\r\n');
				});

				state = 'result';
				str = '';
			}break;

			case 'result':
			{
				if (match === 'error') {
					comm.resultCh('error');
					return;
				}

				let p = match.split(':');
				let cfduid = p[0];
				let clearance = p[1];
				comm.resultCh({
					cfduid,
					clearance
				});
			}break;
		}
	});

	return comm;
};
