'use strict';

module.exports = function (str) {
	this.gatherPart = (regex) => {
		let match = str.match(regex);
		if (match) {
			let r = match[0];
			str = str.substr(r.length).trim();
			return match.length > 1 ? match.slice(1).map(e => e.trim()) : r.trim();
		}

		return '';
	};

	this.getUnmatchedPart = () => str.trim();
};
