'use strict';

const
	url = require('url'),
	redis = require('then-redis');

if (!process.env.REDISCLOUD_URL) {
	console.error('REDISCLOUD_URL environment variable required!');
	process.exit(2);
}

let REDIS_URL = url.parse(process.env.REDISCLOUD_URL.trim());
let host = REDIS_URL.host.split(':');
let auth = REDIS_URL.auth ? REDIS_URL.auth.split(':') : null;

let clients = {};

module.exports = (name, options) => {
	if (!name)
		name = 'default';

	if (clients[name])
		return clients[name];

	if (!options)
		options = {};
	if (!options.port)
		options.port = REDIS_URL.port;
	if (!options.host)
		options.host = host[0];
	if (!options.password && auth)
		options.password = auth[1];

	let client = redis.createClient(options);

	client.on('error', function (err) {
		console.error('redis error ', err);
	});

	process.on('uncaughtException', () => {
		client._redisClient.end();
	});

	process.on('SIGTERM', () => {
		client._redisClient.end();
	});

	clients[name] = client;

	return client;
};
