'use strict';

const
	iCalendar = require('ical.js');

module.exports = function (data, schoolName) {
	let iCalData = iCalendar.parse(data);
	let comp = new iCalendar.Component(iCalData);
	let events = comp.getAllSubcomponents('vevent');

	function parseSummary(summary) {
		let parts = summary.split('-');
		let sport = parts[0], level = parts[1], location = parts[2];
		if (!location)
			return null;

		let r = parts.slice(2).join('-').split('@');
		let name = r[0], loc = r[1];
		let isHome = loc && schoolName.includes(loc);

		let gender = '';
		if (sport) {
			r = sport.replace(/girls/i, '');
			if (r !== sport) {
				gender = 'Girls';
				sport = r;
			} else {
				r = sport.replace(/boys/i, '');
				if (r !== sport) {
					gender = 'Boys';
					sport = r;
				}
			}
		}

		return {
			sport,
			level,
			gender,
			home: isHome ? name : schoolName,
			opponent: isHome ? schoolName : name
		};
	}

	return events
		.map(e => new iCalendar.Event(e))
		.map(e => {
			let summary = parseSummary(e.summary),
				start = e.startDate.toString(),
				end = e.endDate.toString();

			if (!summary)
				return null;

			return {
				url: '',
				summary: e.summary ? e.summary : '',
				sport: summary ? summary.sport : '',
				level: summary ? summary.level : '',
				home: summary ? summary.home : '',
				opponent: summary ? summary.opponent : '',
				gender: summary ? summary.gender : '',
				location: '',
				startDateTime: start,
				endDateTime: end,
				isTimeDefined: true
			};
		});
};
