'use strict';

const
	co = require('co'),
	cheerio = require('cheerio'),
	request = require('../../utils/request'),

	captcha = require('../../utils/captcha'),

	parser = require('./parser'),
	index = require('./index'),

	config = require('../../../config/config.es6'),
	db = require('../../utils/redis.es6')('db');
	//subscriber = require('../utils/redis.es6')('subscriber');

module.exports = function (opts) {
	const self = this;
	const idsRegex = /\[(\d+)\]\s*=\s*(\d+);/ig;
	const nameRegex = (schoolIndex) => new RegExp('\\[' + schoolIndex + ']\\s*=\\s*"(.+)";', 'i');
	const startUrl = 'http://www.oneschoolstreet.com/find_my_school.js.php';

	self.initialize = () => co(function* () {
		let res = '';
		let result = '';
		try {
			res = yield request(startUrl, index.timeout);
		} catch (err) {
			if (err.statusCode === 403) {
				let comm = captcha(startUrl);

				let captchaImage = yield comm.captchaCh;

				yield db.publish(`web-${config.instanceUuid}`, JSON.stringify({
					task: 'captcha',
					captcha: captchaImage
				}));

				let code = yield opts.captchaChan;
				console.log('received resolved code inside loader: ', code);

				yield comm.codeCh(code);
				result = yield comm.resultCh;
				console.log('got result: ', result);

				if (!result.cfduid || !result.clearance)
					return [];

				headers.Cookie = `__cfduid=${result.cfduid}; cf_clearance=${result.clearance}`;

				res = yield request(startUrl, index.timeout);
			}
		}

		let schools = [];

		let match = idsRegex.exec(res.body);
		while (match != null) {
			let schoolIndex = match[1];
			let id = match[2];
			let name = res.body.match(nameRegex(schoolIndex))[1];

			schools.push({
				id,
				name
			});

			match = idsRegex.exec(res.body);
		}

		return schools;
	});

	self.fetch = (school, update) => co(function* (){
		let events = [];

		update(school.name);

		let url = `http://www.oneschoolstreet.com/district.php?district_id=${school.id}`;
		let res = yield request(url, index.timeout);

		let $ = cheerio.load(res.body);

		let schoolsInDistrict = $('select[id=school] option')
			.map(function () {
				let id = $(this).val(),
					name = $(this).text();

				return {
					id: id ? id.trim() : '',
					name: name ? name.trim() : ''
				};
			})
			.get();

		for(let schoolInDistrict of schoolsInDistrict) {
			url = `http://www.oneschoolstreet.com/download_events_ical.php?school_id=${schoolInDistrict.id}`;
			res = yield request(url, index.timeout);

			let content = res.body.trim();

			if (content) {
				try {
					events = events.concat(parser(content, schoolInDistrict.name));
				} catch (err) {
					console.log('PARSE ERROR', err.stack);
				}
			} else
				throw new Error('EMPTY RESPONSE');
		}

		return events;
	});
};
