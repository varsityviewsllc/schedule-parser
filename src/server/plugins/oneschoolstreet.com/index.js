'use strict';

module.exports = {
	name: 'oneschoolstreet.com',
	url: 'http://www.oneschoolstreet.com/find_my_school.php',
	dataFormat: 'iCal',
	concurrency: 1,
	timeout: 60000,
	comment: 'Specify district names you want to get events for',
	title: 'District names',
	options: {
		isFilter: true,
		isStrictMatch: true
	},
	inputListSample: [
		'Fort Zumwalt School District'
	]
};
