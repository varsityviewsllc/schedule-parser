'use strict';

const
	co = require('co'),
	cheerio = require('cheerio'),
	request = require('../../utils/request'),

	index = require('./index');

module.exports = function (opts) {
	const self = this;
	const baseUrl = 'http://apps.roeing.com/raswebschedules/';

	self.initialize = () => co(function* () {
		return opts.inputList;
	});

	self.fetch = (uri, update) => co(function* (){
		let events = [];

		update('mixed');

		let res = yield request(uri, index.timeout);
		let $ = cheerio.load(res.body);

		let entries = $('tr.clsAway,tr.clsHome')
			.map((i1, tr) => ({
				team: $('td:nth-child(1)', tr).text(),
				sport: $('td:nth-child(2)', tr).text(),
				gender: $('td#genderName', tr).text(),
				url: baseUrl + $($('td#url a', tr)).attr('href')
			}))
			.get();

		for(let e of entries) {
			res = yield request(e.url, index.timeout);
			$ = cheerio.load(res.body);

			let list = $('tr.clsAway,tr.clsHome')
				.map((i1, tr) => [$('td', tr).map(
						(i2, td) => $(td).text()).get()])
				.get();

			for (let event of list) {
				let date = event[1] ? event[1].trim() : '';
				let time = event[5] ? event[5].trim() : '';
				let isTimeDefined = time && time.toUpperCase() !== 'TBA';
				let dateTime = new Date(Date.parse(date + (isTimeDefined ? ' ' + time + '+0000' : '')));

				events.push({
					url: e.url,
					summary: '',
					sport: e.sport,
					level: e.team,
					gender: e.gender,
					location: event[4],
					startDateTime: dateTime,
					endDateTime: null,
					isTimeDefined: isTimeDefined,
					home: event[4],
					opponent: event[3]
				});
			}
		}

		return events;
	});
};
