'use strict';

module.exports = {
	name: 'roeing.com',
	url: 'http://apps.roeing.com/',
	dataFormat: 'HTML',
	concurrency: 3,
	timeout: 60000,
	comment: 'Specify school URLs you want to get events for',
	title: 'School URLs',
	options: {
		isFilter: false
	},
	inputListSample: [
		'http://apps.roeing.com/raswebschedules/teamlist.aspx?WebKey=6ea9b977c4',
		'http://apps.roeing.com/raswebschedules/teamlist.aspx?WebKey=27611451CA'
	]
};
