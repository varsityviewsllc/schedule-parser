'use strict';

module.exports = {
	name: '8to18.com',
	url: 'http://ical.8to18.com',
	dataFormat: 'iCalendar',
	concurrency: 1,
	timeout: 60000,
	comment: 'Specify school names you want to get events for',
	title: 'School names',
	options: {
		isFilter: true,
		isStrictMatch: true
	},
	inputListSample: ['Sycamore HS and MS', 'Glenbrook North High School', 'Team of The Month', 'Thompson Jr. High']
};
