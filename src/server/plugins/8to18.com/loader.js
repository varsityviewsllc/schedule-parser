'use strict';

const
	co = require('co'),
	cheerio = require('cheerio'),
	utils = require('../../utils/utils'),
	request = require('../../utils/request'),

	parser = require('./parser'),
	index = require('./index');

module.exports = function () {
	const self = this;

	function buildUri(schoolId, yearFrom, yearTo){
		return 'http://ical.8to18.com/link/' + schoolId + '/' + yearFrom + '-' + yearTo + '/ALL/CDT';
	}

	self.initialize = () => co(function* () {
		let result = yield request('http://ical.8to18.com', index.timeout);

		let $ = cheerio.load(result.body);

		let x = $('select[id=options_school] option')
			.map(function () {
				let id = $(this).val(),
					name = $(this).text();

				return {
					id: id ? id.trim() : '',
					name: name ? name.trim() : ''
				};
			})
			.get();
		console.log(x);
		return x;
	});

	self.fetch = (school, update) => co(function* (){
		let events = [];
		let curYear = utils.getCurrentSchoolYear();

		let uri = buildUri(school.id, curYear, curYear + 1);
		update(school.name);

		let res = yield request(uri, index.timeout);
		let content = res.body.trim();

		if (content) {
			try {
				events = parser(content);
			} catch (err) {
				console.log('PARSE ERROR', err.stack);
				throw new Error('PARSE ERROR');
			}
		} else
			throw new Error('EMPTY RESPONSE');

		return events;
	});
};
