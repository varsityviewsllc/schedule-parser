'use strict';

const
	RegexParser = require('../../utils/regexParser'),
	iCalendar = require('ical.js');

module.exports = function (data) {
	const summary1Regex = /^TIME\s*TBD\s*-\s*/i;
	const summary2Regex = /([a-z\s]+)-/i;
	const summary3Regex = /^(.*)\s*(-\s*VS)\s*/i;
	const summary4Regex = /^([^@]*)\s*(@)\s*/i;
	const locationRegex = /([a-z]+)\s*-\s*(.+)/i;
	const urlRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/i;

	function parseSummary(_summary) {
		let summary = _summary;

		if (!_summary)
			return null;

		let p = new RegexParser(summary);

		let isTBD = !!p.gatherPart(summary1Regex);
		let sport = p.gatherPart(summary2Regex)[0];

		let isHome = false;
		let levelGender = '';

		if (!levelGender) {
			levelGender = p.gatherPart(summary3Regex)[0];
			if (levelGender)
				isHome = false;
		}
		if (!levelGender) {
			levelGender = p.gatherPart(summary4Regex)[0];
			if (levelGender)
				isHome = true;
		}

		let level = '';
		let gender = '';
		if (levelGender) {
			let levelGenderLowerCased = levelGender.toLowerCase();

			if (levelGenderLowerCased.endsWith('boys')) {
				gender = 'Boys';
				level = levelGender.substr(0, levelGender.length - 4).trim();
			} else
			if (levelGenderLowerCased.endsWith('girls')) {
				gender = 'Girls';
				level = levelGender.substr(0, levelGender.length - 5).trim();
			}
			else {
				gender = '';
				level = levelGender.trim();
			}
		}

		let name = p.getUnmatchedPart();

		return {
			isTBD,
			isHome,
			sport,
			level,
			gender,
			name,
			summary
		};
	}

	function parseLocation(location) {
		if (!location)
			return null;

		let matches = location.match(locationRegex);

		return {
			isHome: matches[1].trim().toUpperCase() === 'HOME',
			name: matches[2].trim()
		};
	}

	let iCalData = iCalendar.parse(data);
	let comp = new iCalendar.Component(iCalData);
	let events = comp.getAllSubcomponents('vevent');

	return events
		.map(e => new iCalendar.Event(e))
		.map(e => {
			let summary = parseSummary(e.summary),
				location = parseLocation(e.location),
				description = e.description,
				start = e.startDate.toString(),
				end = e.endDate.toString();

			let urlMatch = description.match(urlRegex);

			return {
				url: urlMatch && urlMatch[0] ? urlMatch[0] : '',
				summary: summary ? summary.summary : '',
				sport: summary ? summary.sport : '',
				level: summary ? summary.level : '',
				gender: summary ? summary.gender : '',
				name: summary ? summary.name : '',
				location: location ? location.name : '',
				startDateTime: start,
				endDateTime: end,
				isTimeDefined: summary ? !summary.isTBD : true,
				isHome: summary ? summary.isHome : false
			};
		});
};
