'use strict';

const
	co = require('co'),
	url	= require('url'),
	cheerio = require('cheerio'),
	utils = require('../../utils/utils'),
	request = require('../../utils/request'),

	parser = require('./parser'),
	index = require('./index');

module.exports = function (opts) {
	const self = this;
	const uriG5genieRegex = /G5genie\/(\d+)/i;
	const uriG5buttonRegex = /G5button\/(\d+)/i;
	const uriSchoolIdRegex = /school_id\/(\d+)/i;

	function buildUri(args) {
		let host = args.host, g5genie = args.g5genie, g5button = args.g5button, schoolId = args.schoolId;

        let d = utils.getDateRange();
        let begin = d[0], end = d[1];

		return `http://${host}/public/conference/downloadnow/`
			+ `G5genie/${g5genie}/G5button/${g5button}/school_id/${schoolId}`
			+ `/category/0/preview/no/vw_activity/0/vw_conference_events/1/vw_non_conference_events`
			+ `/1/vw_homeonly/1/vw_awayonly/1/vw_schoolonly/1/vw_gender/1/vw_type/0/vw_level/0/vw_opponent/0`
			+ `/vw_location_detail/0/opt_show_location/1/opt_show_comments/1/opt_show_bus_times/1/vw_location/0`
			+ `/vw_period/date-range/vw_month_from/${begin.getMonth() + 1}/vw_day_from/${begin.getDay()}/vw_year_from/${begin.getFullYear()}/vw_month_to/${end.getMonth() + 1}/vw_day_to/${end.getDay()}/vw_year_to/${end.getFullYear()}`
			+ `/test/test/isMultipleSchools/1/show_vs_col/on//activity/ffVwLayout/1/multipleSchools/1/downloadtype/ical`;
	}

	function parseUri(uri) {
		let p = url.parse(uri);
		return {
			host: p.host,
			g5genie: uri.match(uriG5genieRegex)[1],
			g5button: uri.match(uriG5buttonRegex)[1],
			schoolId: uri.match(uriSchoolIdRegex)[1]
		};
	}

	self.initialize = () => co(function* () {
		return opts.inputList;
	});

	self.fetch = (uri, update) => co(function* (){
		let events = [];

		let res = yield request(uri, index.timeout);
		let $ = cheerio.load(res.body);
		update($('h1').text().replace(/Calendar\s*-\s*Advanced\s+View/i, '').trim());

		let downloadUri = buildUri(parseUri(uri));
		console.log(downloadUri);

		res = yield request(downloadUri, index.timeout);
		let content = res.body.trim();

		if (content) {
			try {
				events = parser(content);
			} catch (err) {
				console.log('PARSE ERROR', err.stack);
				throw new Error('PARSE ERROR');
			}
		} else
			throw new Error('EMPTY RESPONSE');

		return events;
	});
};
