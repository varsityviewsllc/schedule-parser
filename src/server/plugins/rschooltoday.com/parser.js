'use strict';

const
	RegexParser = require('../../utils/regexParser'),
	iCalendar = require('ical.js');

module.exports = function (data) {
	let iCalData = iCalendar.parse(data);
	let comp = new iCalendar.Component(iCalData);
	let events = comp.getAllSubcomponents('vevent');

	function parseSummary(summary) {
		let p = new RegexParser(summary);

		let sport = p.gatherPart(/^([^:]+):/i)[0];
		let gender = p.gatherPart(/^(girls|boys)/i)[0];
		let level = p.getUnmatchedPart();

		return {
			sport,
			gender,
			level
		};
	}

	function parseDescription(description) {
		let p = new RegexParser(description);

		let r = p.gatherPart(/.*Opponent\s*:\s*(.*)\s*Comments\s*:\s*(.*)/i);
		let name = r[0], comments = r[1];

		let isHome = true;
		if (name) {
			let nameP = new RegexParser(name);

			isHome = !!nameP.gatherPart(/^Away\s+vs\./i);
			name = nameP.getUnmatchedPart();
		}

		return {
			isHome,
			name,
			comments
		};
	}

	function parseLocation(location) {
		if (location)
			return location.trim().toLowerCase() === 'tba' ? '' : location;
		return location;
	}

	return events
		.map(e => new iCalendar.Event(e))
		.map(e => {
			let summary = parseSummary(e.summary),
				location = parseLocation(e.location),
				description = parseDescription(e.description),
				start = e.startDate.toString(),
				end = e.endDate.toString();

			return {
				url: '',
				summary: e.summary,
				sport: summary.sport,
				level: summary.level,
				gender: summary.gender,
				name: description.name,
				isHome: description.isHome,
				location: location,
				startDateTime: start,
				endDateTime: end,
				isTimeDefined: true,
				comments: description.comments
			};
		});
};
