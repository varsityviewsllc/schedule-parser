'use strict';

module.exports = {
	name: 'rschooltoday.com',
	url: 'http://rschooltoday.com/',
	dataFormat: 'iCalendar',
	concurrency: 1,
	timeout: 60000,
	comment: 'Specify school URLs you want to get events for',
	title: 'School URLs',
	options: {
		isFilter: false
	},
	inputListSample: ['http://srv2.advancedview.rschooltoday.com/public/conference/calendaradvance/G5statusflag//G5button/13/G5genie/758/G5MID//school_id/758']
};
