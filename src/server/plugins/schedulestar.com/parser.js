'use strict';

module.exports = function (data) {
	data = JSON.parse(data);

	return data.EVENTLIST.map(e => {
		let isTimeDefined = e.STARTTIME && e.STARTTIME.toUpperCase() !== 'TBA';
		let start = isTimeDefined
			? e.EVENTDATE.replace('00:00:00', e.STARTTIME.trim().replace(/(am|pm)$/, ' $1'))
			: e.EVENTDATE;
		let isHome = e.HOMEAWAY === 'H';

		return {
			url: '',
			summary: '',
			description: '',
			sport: e.SPORT,
			level: e.LEVEL,
			gender: e.GENDER,
			name: e.OPPONENT_NAME, // name serves the opposite
			isHome: !isHome,	   // so we should flip isHome flag
			location: isHome ? e.MY_ADDRESS : e.YOUR_ADDRESS,
			startDateTime: new Date(Date.parse(start + '+0000')),
			endDateTime: '',
			isTimeDefined: isTimeDefined,
			comments: ''
		};
	});
};
