'use strict';

const
	co = require('co'),
	utils = require('../../utils/utils'),
	request = require('../../utils/request'),

	parser = require('./parser'),
	index = require('./index');

module.exports = function () {
	const self = this;

	function buildUri(schoolId, month, year){
		return `http://schedules.schedulestar.com/cfcs/schedule.cfc?ReturnFormat=json&method=getEventList&sc_id=${schoolId}&schedDate=${month}.01.${year}&current_schedule_view=month&userid=0`;
	}

	self.initialize = () => co(function* () {
		let res = yield request('http://schedules.schedulestar.com/cfcs/sso.cfc?ReturnFormat=json&method=findSchoolByName&searchString=', index.timeout);

		return JSON.parse(res.body).RESULTS.DATA
			.map(e => ({
				id: e[0],
				name: e[1]
			}));
	});

	self.fetch = (school, update) => co(function* (){
		let events = [];

		update(school.name);

        let d = utils.getDateRange();
        let begin = d[0], end = d[1];
        console.log(begin, end);

		for(let month = begin.getMonth(), year = begin.getFullYear(); month <= end.getMonth() && year <= end.getFullYear(); month++) {
			let uri = buildUri(school.id, month + 1, year);

			if (month >= 11) {
				month = 0;
				year++;
			}

			console.log(uri);

			let monthEvents = [];

			let res = yield request(uri, index.timeout);
			let content = res.body.trim();

			if (content) {
				try {
					monthEvents = parser(content);
					events = events.concat(monthEvents);
				} catch (err) {
					console.log('PARSE ERROR', err.stack);
					throw new Error('PARSE ERROR');
				}
			} else
				throw new Error('EMPTY RESPONSE');
		}

		return events;
	});
};
