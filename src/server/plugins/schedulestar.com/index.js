'use strict';

module.exports = {
	name: 'schedulestar.com',
	url: 'http://schedules.schedulestar.com/',
	dataFormat: 'JSON',
	concurrency: 1,
	timeout: 60000,
	comment: 'Specify school names you want to get events for',
	title: 'School names',
	options: {
		isFilter: true,
		isStrictMatch: true
	},
	inputListSample: ['Brantley HS', 'Autaugaville HS', 'Niles North High School']
};
