'use strict';

require('../utils/env.es6');

process.on('uncaughtException', function(err) {
	console.error('web uncaught exception: ', err.stack);
	process.exit(1);
});

const
	path = require('path'),
	http = require('http'),
	childProcess = require('child_process'),

	auth = require('koa-basic-auth'),
	cors = require('koa-cors'),
	koa = require('koa'),
	route = require('koa-route'),
	staticCache = require('koa-static-cache'),

	config = require('../../config/config.es6');

const
	routes = ['delete.result', 'get.info', 'get.plugin', 'get.result', 'get.stats', 'post.captcha', 'post.job'];

childProcess.fork(path.join(__dirname, './worker.js'), [config.target, config.instanceUuid]);

(() => {
	let app = koa();

	let servePath = path.resolve(__dirname, '..', '..', '.' + config.outputClient.replace(config.output, ''));

	console.log('web: serve ', servePath);

	if (config.isProduction) {
		app.use(function* (next) {
			try {
				yield next;
			} catch (err) {
				if (err.status === 401) {
					this.status = 401;
					this.set('WWW-Authenticate', 'Basic');
					this.body = 'cant haz that';
				} else {
					throw err;
				}
			}
		});

		app.use(auth({name: process.env.WEB_LOGIN, pass: process.env.WEB_PASSWORD}));
	}

	if (config.isDevelop) {
		console.log(`(develop) allow cors from: http://${config.webRealHost}:${config.devPort}`);
		app.use(cors({
			origin: `http://${config.webRealHost}:${config.devPort}`
		}));
	}

	app.use(function* (next) {
		if (this.request.url === '/')
			this.request.url = '/index.html';

		//console.log('serve', this.request.url);
		this.response.set('X-Powered-By', config.devServerName);
		yield next;
	});

	app.use(staticCache(servePath + '/assets', {
		dir: '/assets',
		maxAge: 365 * 24 * 60 * 60,
		gzip: true,
		usePrecompiledGzip: true,
		buffer: true
	}));

	app.use(staticCache(servePath, {
		dir: '/',
		maxAge: 0,
		gzip: true,
		usePrecompiledGzip: true,
		buffer: true
	}));

	for(let r of routes) {
		let method = path.basename(r).split('.')[0];
		let rd = require(`../routes/${r}.js`);
		let routePath = Object.keys(rd)[0];
		console.log(`create route ${routePath}...`);
		app.use(route[method](routePath, rd[routePath]));
	}

	let server = http.createServer(app.callback());

	process.on('uncaughtException', () => {
		server.close();
		setTimeout(() => process.exit(), 100);
	});

	process.on('SIGTERM', () => {
		server.close();
		setTimeout(() => process.exit(), 100);
	});

	server.listen(config.webPort, config.webHost);

	console.log(config.webHost + ':' + config.webPort, 'started...');
})();
