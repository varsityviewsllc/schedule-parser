'use strict';

require('../utils/env.es6');

process.on('uncaughtException', err => {
	console.error('worker uncaught exception: ', err.stack);
	process.exit(1);
});

const
	co = require('co'),
	chan = require('chan'),
	limiter = require('co-limiter'),
	subscriber = require('../utils/redis.es6')('subscriber'),
	config = require('../../config/config.es6'),
	entryStore = require('../worker/store');

let captchaChan = chan();

function startJob(inputList, databaseName, isStrictMatch) {
	if (!inputList)
		inputList = [];

	co(function* (){
		try {
			yield entryStore.setStatus('working');

			let Loader = require('../plugins/' + databaseName + '/loader');
			let index = require('../plugins/' + databaseName + '/index');
			let nameSchoolFilter = require('../worker/nameSchoolFilter');

			let loader = new Loader({
				inputList,
				captchaChan
			});

			let list = yield loader.initialize();
			let filteredList = index.options.isFilter
				? nameSchoolFilter(list, inputList, isStrictMatch)
				: list;
			console.log('processing entries: ', filteredList);

			let onStart = (args) => {
				entryStore.storeSchoolEvents({
					dataFormat: args.dataFormat,
					name: args.name,
					status: args.status
				});
			};

			let onProgress = (args) => {
				console.log(args.status, args.dataFormat, args.entry, '"' + args.name + '":', args.events.length + ' event(s) found...');

				entryStore.storeSchoolEvents({
					dataFormat: args.dataFormat,
					name: args.name,
					status: args.status,
					events: args.events
				});
			};

			let limit = limiter(index.concurrency);
			yield filteredList.map(entry => co(function* () {
				yield limit(function* () {
					let info = {
						schoolName: 'unknown',
						status: 'IN_PROGRESS'
					};
					let update = (schoolName) => {
						info.schoolName = schoolName;
						onStart({
							dataFormat: index.dataFormat,
							name: info.schoolName,
							status: info.status
						});
					};

					let events = [];

					try {
						events = yield loader.fetch(entry, update);
						info.status = 'OK';
					} catch (err) {
						let errorCode = err && err.error && err.error.code ? err.error.code.toUpperCase() : '';
						let isTimeout = errorCode === 'EJSTIMEDOUT' || errorCode === 'ETIMEDOUT' || errorCode === 'ESOCKETTIMEDOUT';

						info.status = isTimeout ? 'E_TIMEOUT' : (err.statusCode ? 'E_HTTP ' + err.statusCode : errorCode);
						if (!info.status)
							info.status = err.message;
					}

					onProgress({
						entry,
						dataFormat: index.dataFormat,
						name: info.schoolName,
						status: info.status,
						events
					});
				});
			}));
		} catch (err) {
			console.error('worker start job error: ', err.stack);
		} finally {
			yield entryStore.setStatus('idle');
		}
	});
}

subscriber.on('message', function (channel, message) {
	message = JSON.parse(message);
	console.log('worker received message: ', message);

	switch (message.task) {
		case 'job.start':
			startJob(message.inputList, message.databaseName, message.isStrictMatch);
			break;

		case 'captcha.code':
			captchaChan(message.code);
			break;

		default:
			console.error('worker got undefined redis task', message.task);
			break;
	}
});

co(function* (){
	try {
		yield entryStore.setStatus('idle');

		subscriber.subscribe(`worker-${config.instanceUuid}`);
	} catch (err) {
		console.error('worker initialize error: ', err.stack);
	}
});
