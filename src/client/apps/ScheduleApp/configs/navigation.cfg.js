module.exports = ($stateProvider, $urlRouterProvider, $locationProvider) => {
	$locationProvider.html5Mode(true);

	$urlRouterProvider.otherwise('/');

	const primaryStates = {
		'list': {
			url: '/',

			views: {
				'main-view': {
					templateUrl: './views/main/main',
					controller: 'CtrlMain'
				}
			}
		}
	};

	function PopupAbstractState () {
		this.abstract = true;
	}

	const popupStates = {
	};

	const declareState = (name, state) => {
		console.log('creating state ', name);
		$stateProvider.state(name, state);
	};

	for(let stateName of Object.keys(primaryStates)) {
		declareState(stateName, primaryStates[stateName]);

		if (!primaryStates[stateName].abstract) {
			declareState(`${stateName}.popup`, new PopupAbstractState());
			for (let popupStateName in popupStates)
				declareState(`${stateName}.popup.${popupStateName}`, new popupStates[popupStateName]());
		}
	}
};
