module.exports = {
	API_URI: env.TARGET === 'production' ? '/api/v1' : 'http://localhost:5000/api/v1'
};
