const
	co = require('co');

module.exports = (coProvider) => {
	coProvider.coJS = co;
};
