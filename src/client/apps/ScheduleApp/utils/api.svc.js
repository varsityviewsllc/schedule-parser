module.exports = function ($http, co, consts) {
	this.get = (uri, query) => co(function* (){
		return yield $http({
			url: `${consts.API_URI}/${uri}`,
			method: 'GET',
			params: query
		});
	});

	this.post = (uri, query) => co(function* (){
		return yield $http({
			url: `${consts.API_URI}/${uri}`,
			method: 'POST',
			params: query
		});
	});

	this.del = (uri, query) => co(function* (){
		return yield $http({
			url: `${consts.API_URI}/${uri}`,
			method: 'DELETE',
			params: query
		});
	});

};
