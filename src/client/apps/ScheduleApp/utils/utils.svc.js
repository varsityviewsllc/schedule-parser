const
	sleep = require('co-sleep');


const fileSaver = require('../../../_helpers/fileSaver');

module.exports = function (co) {
	const self = this;

	this.capitalize = (name) => name.substr(0, 1).toUpperCase() + name.substr(1);

	this.download = (data, name) => {
		let blob = new Blob([data], {type: 'text/plain;charset=utf-8'});
		fileSaver.saveAs(blob, name);
	};

	this.def = (call, def) => {
		try {
			return call();
		} catch(err) {
			return def;
		}
	};

	this.uniq = (array, key = null) => {
		if (!key)
			key = c => c;

		return [...array.reduce((map, c) => {
			map.set(key(c), c);
			return map;
		}, new Map()).values()];
	};

	this.uniqMap = (array, map) => self.uniq(array.map(map));

	this.toArray = (obj) => {
		return Object.keys(obj).reduce((a, k) => {
			a.push(obj[k]);
			return a;
		}, []);
	};

	this.toMap = (array, key, map) => {
		if (!key)
			key = (e) => e.id;
		if (!map)
			map = (e) => e;

		return array.reduce((a, t) => {
			a[key(t)] = map(t);
			return a;
		}, {});
	};

	this.sleep = (time) => co(function* (){
		yield sleep(time);
	});

	this.wait = (condition, checkTimeout = 100) => co(function* (){
		while (!condition()) {
			yield self.sleep(checkTimeout);
		}
	});
};
