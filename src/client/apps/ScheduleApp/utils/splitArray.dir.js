module.exports = () => ({
	restrict: 'A',
	require: 'ngModel',
	link: (scope, element, attr, ngModel) => {
		ngModel.$parsers.push(text => text.split('\n').filter(e => e));
		ngModel.$formatters.push(array => array.join('\n'));
	}
});
