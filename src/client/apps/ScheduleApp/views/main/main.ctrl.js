require('./main.less');

module.exports = ($scope, co, api, utils) => {
	$scope.plugins = [];
	$scope.databaseName = '';

	$scope.isStrictMatch = false;
	$scope.inputList = [];
	$scope.statusBoard = [];
	$scope.status = 'idle';
	$scope.captcha = '';
	$scope.captchaCode = '';

	$scope.version = '';

	co(function* (){
		while (true)
			try {
				let r = yield api.get('job-status');
				$scope.statusBoard = r.data.statusBoard;
				if (r.data.status) {
					$scope.status = r.data.status;
				}
				$scope.captcha = r.data.captcha ? 'data:image/png;base64,' + r.data.captcha : '';
			} catch (err) {
				console.warn('error during job-status', err);
			} finally {
				yield utils.sleep(1000);
			}
	});

	$scope.resolveCaptcha = () => co(function* (){
		yield api.post('captcha', {
			code: $scope.captchaCode
		});
	});

	$scope.startJob = () => co(function* (){
		$scope.status = 'queuing new job';


		yield api.post('job', {
			isStrictMatch: $scope.isStrictMatch,
			inputList: $scope.inputList,
			databaseName: $scope.databaseName
		});
	});

	$scope.isDownloading = false;
	$scope.downloadCsv = () => co(function* (){
		$scope.isDownloading = true;
		try {
			let r = yield api.get('result');

			utils.download(r.data, 'schedule.csv');
		} finally {
			$scope.isDownloading = false;
		}
	});

	$scope.clearData = () => co(function* (){
		yield api.del('result');
	});

	$scope.$watch('databaseName', () => {
		if ($scope.plugins[$scope.databaseName])
			$scope.inputList = $scope.plugins[$scope.databaseName].inputListSample;
	});

	co(function* (){
		let r = yield api.get('plugin');
		$scope.plugins = r.data.list;
		$scope.databaseName = $scope.plugins[Object.keys($scope.plugins)[0]].name;

		r = yield api.get('info');
		$scope.version = r.data.version;
	});
};
