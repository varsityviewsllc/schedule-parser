'use strict';

require('./less/main.less');
require('./index.jade');

let AngularApplication = require('angularApplication');

require('babel-polyfill');
require('angular-bootstrap');
require('angular-ui-router');
require('angular-co');

let app = new AngularApplication({
	applicationName: 'ScheduleApp',
	dependencies: ['ui.router', 'ui.bootstrap', 'angular-co']
});
app.registerBulks(require.context('./apps/ScheduleApp', true));
