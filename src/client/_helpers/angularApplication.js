let angular = require('angular');
let path = require('path');

function AngularApplication ({applicationName, dependencies, productionOnlyDependencies}) {
	if (!dependencies)
		dependencies = [];
	if (!productionOnlyDependencies)
		productionOnlyDependencies = [];

	let actualDependencies = (env.TARGET === 'production' ? productionOnlyDependencies : []).concat(dependencies);

	const capitalize = (name) => name.substr(0, 1).toUpperCase() + name.substr(1);

	console.debug(`AngularApplication ${applicationName} create:`, actualDependencies);

	let applicationModule = null;
	let templateCache = {};

	applicationModule = angular.module(applicationName, actualDependencies);


	function register(name, type, m) {
		switch (type) {
			case 'run':
				console.debug(`AngularApplication ${applicationName} register: new run`, name);
				applicationModule.run(m);
				break;
			case 'config':
			case 'cfg':
				console.debug(`AngularApplication ${applicationName} register: new config`, name);
				applicationModule.config(m);
				break;
			case 'constant':
			case 'const':
				console.debug(`AngularApplication ${applicationName} register: new constant`, name);
				applicationModule.constant(name, m);
				break;
			case 'decorator':
			case 'deco':
				console.debug(`AngularApplication ${applicationName} register: new decorator`, name);
				applicationModule.config(/*@ngInject*/ ($provide) => {
					$provide.decorator(name, m);
				});
				break;
			case 'filter':
			case 'fltr':
				console.debug(`AngularApplication ${applicationName} register: new filter`, name);
				applicationModule.filter(name, m);
				break;
			case 'directive':
			case 'dir':
				console.debug(`AngularApplication ${applicationName} register: new directive`, name);
				applicationModule.directive(name, m);
				break;
			case 'factory':
			case 'fact':
				let declarativeFactoryName = capitalize(name);
				console.debug(`AngularApplication ${applicationName} register: new factory`, declarativeFactoryName);
				applicationModule.factory(declarativeFactoryName, m);
				break;
			case 'service':
			case 'svc':
				console.debug(`AngularApplication ${applicationName} register: new service`, name);
				applicationModule.service(name, m);
				break;
			case 'controller':
			case 'ctrl':
				let declarativeControllerName = 'Ctrl' + capitalize(name);
				console.debug(`AngularApplication ${applicationName} register: new controller`, declarativeControllerName);
				applicationModule.controller(declarativeControllerName, m);
				break;
			default:
				console.warn(`AngularApplication ${applicationName} register: unknown entity`, name, 'of type', type);
				break;
		}
	}

	this.registerBulks = (angularBulks) => {
		console.log(`AngularApplication ${applicationName}: registerBulks`, angularBulks);
		for (let k of angularBulks.keys()) {

			if (k.endsWith('.js')) {
				let parts = path.basename(k).split('.');

				let name = parts[0];
				let type = parts[1];

				register(name, type, angularBulks(k));
			} else
			if (k.endsWith('.jade')) {
				templateCache[path.dirname(k) + '/' + path.basename(k, path.extname(k))] = angularBulks(k);
			}
		}

		applicationModule.run(/* @ngInject */($templateCache) => {
			for(let name of Object.keys(templateCache)) {
				console.debug(`AngularApplication ${applicationName}: registering template in $templateCache`, name);
				$templateCache.put(name, templateCache[name]);
			}
		});

		let appContainer = document.getElementById(applicationName);
		console.log(appContainer);
		angular.element(appContainer).ready(() => {
			angular.bootstrap(appContainer, [applicationName], {
				strictDi: true
			});

			console.log(`AngularApplication ${applicationName} bootstrapped!`);
		});

		return this;
	};
}

module.exports = AngularApplication;
