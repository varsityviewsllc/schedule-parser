var system = require('system');
var casper = require('casper').create({
	/*verbose: true,
	logLevel: 'debug',*/
	waitTimeout: 20000,
	viewportSize: {
		width: 1920,
		height: 1080
	}
});

var url = casper.cli.args[0];
if (!url) {
	console.error('url required');
	casper.exit();
}

casper.start(url);

casper.then(function() {
	casper.page.customHeaders = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
		'Cache-Control': 'no-cache',
		'Pragma': 'no-cache',
		'Connection': 'keep-alive'
	};
});

casper.waitForResource(/recaptcha\/api\/image/, function (){
	var bounds = this.getElementBounds('#recaptcha_challenge_image');
	var captcha = this.captureBase64('png', bounds);
	console.log('!' + captcha + '!');
	var code = system.stdin.readLine();
	console.log('got code: |' + code + '|');

	this.fillSelectors('#challenge-form', {
		'#recaptcha_response_field': code
	}, true);

	this.waitFor(function(){
		var clearance = phantom.cookies.filter(function(c) {
			return c.name == 'cf_clearance';
		})[0];

		var cfduid = phantom.cookies.filter(function(c) {
			return c.name == '__cfduid';
		})[0];

		if (!clearance || !cfduid)
			return false;

		console.log('!' + cfduid.value + ':' + clearance.value + '!');
		return true;
	}, function(){

	}, function(){
		console.log('!error!');
	}, 5000);
});

casper.run();