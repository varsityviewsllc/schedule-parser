'use strict';

const
	fs = require('fs'),
	path = require('path'),

	fsExtra = require('fs-extra'),
	co = require('co'),
	chan = require('chan'),
	ProgressBar = require('progress'),
	console = require('better-console'),
	ncp = require('ncp').ncp,
	
	WebpackDevServer = require('webpack-dev-server'),
	webpack = require('webpack'),
	supervisor = require('supervisor/lib/supervisor.js'),

	config = require('../config/config.es6'),
	clientConfig = require('../config/webpack.config.es6');

let webpackLog = (err, stats) => {
	console.log(stats.toString({colors: true}));
};

let bar = new ProgressBar(`Perform build(${config.target}): :percent :msg`, { total: 100 });
let builds = {};

function createCompiler(name, webpackConfig, ch) {
	let compiler = webpack(webpackConfig);
	builds[name] = {
		percent: 0
	};

	compiler.apply(new webpack.ProgressPlugin((percentage, msg) => {
		builds[name].percent = percentage;

		let buildKeys = Object.keys(builds);
		let totalPercentage = buildKeys.reduce((a, c) => {
			a += builds[c].percent;
			return a;
		}, 0) / buildKeys.length;

		bar.update(totalPercentage, {
			msg,
			name
		});

		if (percentage >= 1) {
			if (ch)
				ch(true);
		}
	}));

	return compiler;
}

function bumpPackage() {
	let p = require(path.resolve('./package.json'));
	let rPath = './src/_redistributable/package.json';
	let pkg = JSON.parse(fs.readFileSync(rPath, 'utf8'));
	let v = pkg.version.split('.');
	v[v.length - 1]++;
	pkg.version = v.join('.');
	pkg.dependencies = p.dependencies;
	fs.writeFileSync(rPath, JSON.stringify(pkg, null, 2));
}

let clientCompiler = createCompiler('client', clientConfig, () => {
	bumpPackage();

	fsExtra.copySync('./src/_redistributableClient', config.outputClient);
	fsExtra.copySync('./src/_redistributable', config.output);
});

if (config.isProduction) {
	clientCompiler.run(webpackLog);
	ncp(config.inputServer, config.outputServer);
	ncp(config.inputConfig, config.outputServer + '/../config');
} else {
	co(function* (){
		supervisor.run(['-q', '-n', 'error', '-w', config.inputServer, '--harmony', '--', `${config.inputServer}/apps/web.js`, config.target]);
	});
}

if (config.isDevelop) {
	let server = new WebpackDevServer(clientCompiler, clientConfig.devServer);
	server.listen(clientConfig.devServer.port, clientConfig.devServer.host);
}