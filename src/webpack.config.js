require('babel-core/register')({
	extensions: ['.es6']
});

module.exports = require('./config/webpack.config.es6');