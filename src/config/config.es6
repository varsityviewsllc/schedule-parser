'use strict';

let uuid = require('node-uuid');
let target = process.env.TARGET = process.argv[2];
let sharedUuid = process.argv[3];

if (target != 'production' && target != 'develop') {
	console.error('Invalid target', target);
	process.exit();
}

let config = {
	target: target,
	isProduction: target == 'production',
	isDevelop: target == 'develop',

	devPort: 3000,
	devHost: '0.0.0.0',
	realHost: 'localhost',
	devServerName: 'Darth Vader',

	webPort: process.env.PORT || 5000,
	webHost: '0.0.0.0',
	webRealHost: 'localhost',

	instanceUuid: sharedUuid ? sharedUuid : uuid.v1()
};

config.inputServer = './src/server';
config.inputConfig = './src/config';
config.output = config.isProduction ? './dist-production' : './dist-develop';
config.outputServer = config.output + '/server';
config.outputClient = config.output + '/public';

module.exports = config;