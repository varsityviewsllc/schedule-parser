'use strict';

const
	fs = require('fs'),
	path = require('path'),
	jade = require('jade'),

	webpack = require('webpack'),
	HtmlPlugin = require('html-webpack-plugin'),
	ExtractTextPlugin = require('extract-text-webpack-plugin'),
	BowerPlugin = require('bower-webpack-plugin'),
	NgAnnotatePlugin = require('ng-annotate-webpack-plugin'),
	StringReplacePlugin = require('string-replace-webpack-plugin'),
	CompressionPlugin = require('compression-webpack-plugin');

const
	config = require('./config.es6'),
	vendorLoaders = require('./vendor.loaders.config.es6');

let clientConfig = {
	devServer: {
		contentBase: path.resolve(config.outputClient),
		headers: {
			'X-Powered-By': config.devServerName
		},
		host: config.devHost,
		port: config.devPort,

		watchOptions: {
			aggregateTimeout: 300,
			poll: 100
		},

		hot: false,
		stats: { cached: false, cachedAssets: false, colors: true }
	},

	devtool: 'sourcemap',

	entry: {
		app: [path.resolve('./src/client/main.js')],
		//vendor: ['angular', 'angular-ui-router']
	},

	output: {
		path: path.resolve(config.outputClient + (config.isProduction ? '/assets' : '')),
		publicPath: config.isProduction ? '/assets/' : '/',
		filename: 'app.[hash].js',
		chunkFilename: 'app-chunk-[id].[hash].js'
	},

	module: {
		preLoaders: [
			{
				test: /\/apps\/.*\.js$/,
				loader: StringReplacePlugin.replace({
					replacements: [
						{
							pattern: /module\.exports\s*=\s*\(/g,
							replacement: () => 'module.exports = /*@ngInject*/ ('
						},
						{
							pattern: /module\.exports\s*=\s*function\s*\(/g,
							replacement: () => 'module.exports = /*@ngInject*/ function('
						}
					]
				})
			},
			{
				test: /index\.jade$/,
				loader: StringReplacePlugin.replace({
					replacements: [
						{
							pattern: /.*/ig,
							replacement: () => ''
						}
					]
				})
			}
		],
		loaders: [...vendorLoaders, ...[
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loaders: [
					'babel?presets[]=es2015',
					'eslint'
				]
			},
			{
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: 'url-loader?limit=10000&minetype=application/font-woff'
			},
			{
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: 'file-loader'
			},
			{
				test: /\.jade$/,
				loader: 'html!jade-html?doctype=html'
			},
			{
				test: /\.less$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader!autoprefixer-loader?browsers=last 2 versions!less-loader')
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				loaders: [
					'url?limit=8192',
					'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
				]
			}
		]]
	},

	resolve: {
		alias: {
			angularApplication: path.resolve('./src/client/_helpers/angularApplication.js')
		},
		extensions: ['', '.webpack.js', '.web.js', '.js', '.woff'],
		root: [path.join(__dirname, '../..', 'bower_components')]
	},

	plugins: [
		new StringReplacePlugin(),
		new ExtractTextPlugin('[name].[hash].css'),
		new HtmlPlugin({
			templateContent: function(templateParams, compilation, callback) {
				let fn = jade.compile(fs.readFileSync('./src/client/index.jade'), {doctype: 'html'});

				let locals = {
					jsAssets: templateParams.htmlWebpackPlugin.files.js.filter(jsPath => {
						let js = path.basename(jsPath);
						return js.startsWith('vendor.') || js.startsWith('app.');
					}),
					cssAssets: templateParams.htmlWebpackPlugin.files.css
				};

				return fn(locals);
			},
			filename: config.isProduction ? '../index.html' : 'index.html',
			minify: config.isProduction ? {} : false
		}),
		//new webpack.optimize.CommonsChunkPlugin(/* chunkName= */'vendor', /* filename= */'vendor.[hash].js'),
		new BowerPlugin(),
		new NgAnnotatePlugin({
			add: true
		}),
		new webpack.DefinePlugin({
			env: {
				TARGET: JSON.stringify(process.env.TARGET)
			}
		})
	]
};

if (config.isProduction) {
	clientConfig.plugins.push(new CompressionPlugin({
		asset: '{file}.gz',
		algorithm: 'gzip',
		regExp: /\.(jpg|jpeg|png|gif|js|css|html)$/,
		minRatio: 0.95
	}));

	clientConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({
		compress: {
			warnings: false
		}
	}));
}

module.exports = clientConfig;