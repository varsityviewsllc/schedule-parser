'use strict';

const
	del = require('del'),

	config = require('./config.es6');

del.sync(config.output + '/**/*');

module.exports = require('./webpack.client.config.es6');